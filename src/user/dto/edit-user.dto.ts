import { IsEmail, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class EditUserDto {
  @IsEmail()
  @IsOptional()
  @ApiProperty({ type: String, description: 'email' })
  email?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'firstName' })
  firstName?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'lastName' })
  lastName?: string;
}
