import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'This is API for Blog Post for the docs you can check on /docs !';
  }
}
