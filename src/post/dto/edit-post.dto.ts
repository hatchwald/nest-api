import { IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class EditPostDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'title' })
  title?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'content' })
  content?: string;
}
