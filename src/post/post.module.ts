import { Module } from '@nestjs/common';
import { postController } from './post.controller';
import { PostService } from './post.service';

@Module({
  controllers: [postController],
  providers: [PostService],
})
export class PostModule {}
