import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreatePostDto, EditPostDto } from './dto';

@Injectable()
export class PostService {
  constructor(private prisma: PrismaService) {}

  getPosts(userId: number) {
    return this.prisma.post.findMany({
      where: {
        userId,
      },
    });
  }

  getPostById(userId: number, PostId: number) {
    return this.prisma.post.findFirst({
      where: {
        id: PostId,
        userId,
      },
    });
  }

  async createPost(userId: number, dto: CreatePostDto) {
    const Post = await this.prisma.post.create({
      data: {
        userId,
        ...dto,
      },
    });

    return Post;
  }

  async editPostById(userId: number, PostId: number, dto: EditPostDto) {
    // get the Post by id
    const Post = await this.prisma.post.findUnique({
      where: {
        id: PostId,
      },
    });

    // check if user owns the Post
    if (!Post || Post.userId !== userId)
      throw new ForbiddenException('Access to resources denied');

    return this.prisma.post.update({
      where: {
        id: PostId,
      },
      data: {
        ...dto,
      },
    });
  }

  async deletePostById(userId: number, PostId: number) {
    const Post = await this.prisma.post.findUnique({
      where: {
        id: PostId,
      },
    });

    // check if user owns the Post
    if (!Post || Post.userId !== userId)
      throw new ForbiddenException('Access to resources denied');

    await this.prisma.post.delete({
      where: {
        id: PostId,
      },
    });
  }
}
