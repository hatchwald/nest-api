import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { GetUser } from '../auth/decorator';
import { JwtGuard } from '../auth/guard';
import { PostService } from './post.service';
import { CreatePostDto, EditPostDto } from './dto';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';

@UseGuards(JwtGuard)
@Controller('posts')
export class postController {
  constructor(private postService: PostService) {}

  @Get()
  @ApiCreatedResponse({ description: 'Get posts list' })
  getposts(@GetUser('id') userId: number) {
    return this.postService.getPosts(userId);
  }

  @Get(':id')
  getpostById(
    @GetUser('id') userId: number,
    @Param('id', ParseIntPipe) postID: number,
  ) {
    return this.postService.getPostById(userId, postID);
  }

  @Post()
  @ApiCreatedResponse({ description: 'Submit A Post' })
  @ApiBody({ type: CreatePostDto })
  createpost(@GetUser('id') userId: number, @Body() dto: CreatePostDto) {
    return this.postService.createPost(userId, dto);
  }

  @Patch(':id')
  @ApiCreatedResponse({ description: 'update A post' })
  @ApiBody({ type: EditPostDto })
  editpostById(
    @GetUser('id') userId: number,
    @Param('id', ParseIntPipe) postID: number,
    @Body() dto: EditPostDto,
  ) {
    return this.postService.editPostById(userId, postID, dto);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deletepostById(
    @GetUser('id') userId: number,
    @Param('id', ParseIntPipe) postID: number,
  ) {
    return this.postService.deletePostById(userId, postID);
  }
}
