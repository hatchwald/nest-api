import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "This is API for Blog Post for the docs you can check on /docs !"', () => {
      expect(appController.getHello()).toBe(
        'This is API for Blog Post for the docs you can check on /docs !',
      );
    });
  });
});
